package imat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author borge
 */
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import se.chalmers.ait.dat215.project.*;

public class Model {

    private static Model instance = null;
    private IMatDataHandler iMatDataHandler;

    protected Model() {
    }

    public static Model getInstance() {
        if (instance == null) {
            instance = new Model();
            instance.init();
        }
        return instance;
    }

    private void init() {

        iMatDataHandler = IMatDataHandler.getInstance();
        sci = new ArrayList();
    }

    public Product getProduct(int idNbr) {
        return iMatDataHandler.getProduct(idNbr);
    }

    public java.util.List<Product> getProducts(ProductCategory pc) {
        return iMatDataHandler.getProducts(pc);
    }

    public ImageIcon getImageIcon(Product p, Dimension d) {
        return getImageIcon(p, d.width, d.height);
    }

    public ImageIcon getImageIcon(Product p, int width, int height) {
        return iMatDataHandler.getImageIcon(p, width, height);
    }

    public void addToShoppingCart(Product p, int n) {
        ShoppingCart shoppingCart = iMatDataHandler.getShoppingCart();
        Boolean bool =false;
        //TODO: add to shoppingcart panel.
        if (n > 0) {
            ShoppingItem shi = new ShoppingItem(p, n);
            if (sci.isEmpty()) {
                shoppingCart.addItem(shi);
                sci.add(shi);
            } 
            else {
                for (int i = 0; i < sci.size(); i++){
                    if(sci.get(i).getProduct().equals(p)){
                        increaseAmount(shoppingCart, n, i);
                        bool=true;
                    }
                }
                if (!bool) {
                    shoppingCart.addItem(shi);
                    sci.add(shi);

                    
                }

            }
        }
    }
    
    public void increaseAmount(ShoppingCart shoppingCart, int n, int indx){
        //indx = shoppingCart.getItems().indexOf(sci);
        Double oldAmount;
        oldAmount = shoppingCart.getItems().get(indx).getAmount();
        shoppingCart.getItems().get(indx).setAmount(oldAmount + n);
        shoppingCart.fireShoppingCartChanged(shoppingCart.getItems().get(indx), true);
    }

    public void resetRun() {
        if (!iMatDataHandler.isFirstRun()) {
            iMatDataHandler.reset();
            iMatDataHandler.resetFirstRun();
        }
    }

    public Customer getCustomer() {
        return iMatDataHandler.getCustomer();
    }

    public ShoppingCart getShoppingCart() {
        return iMatDataHandler.getShoppingCart();
    }

    public void clearShoppingCart() {
        iMatDataHandler.getShoppingCart().clear();
    }

    public void placeOrder() {
        iMatDataHandler.placeOrder();
    }

    public int getNumberOfOrders() {
        return iMatDataHandler.getOrders().size();
    }

    public java.util.List<Product> findProduct(String s) {
        return iMatDataHandler.findProducts(s);
    }

    public String getAddress() {
        return getCustomer().getAddress();
    }

    public void setAddress(String address) {
        getCustomer().setAddress(address);
    }

    public void removeItem(ShoppingItem sci) {
        rmIndex = iMatDataHandler.getShoppingCart().getItems().indexOf(sci);
        iMatDataHandler.getShoppingCart().removeItem(sci);
    }

    public String getEmail() {
        return getCustomer().getEmail();
    }

    public void setEmail(String email) {
        getCustomer().setEmail(email);
    }

    public String getFirstName() {
        return getCustomer().getFirstName();
    }

    public void setFirstName(String firstname) {
        getCustomer().setFirstName(firstname);
    }

    public String getLastName() {
        return getCustomer().getLastName();
    }

    public void setLastName(String lastname) {
        getCustomer().setLastName(lastname);
    }

    public String getPhoneNumber() {
        return getCustomer().getPhoneNumber();
    }

    public void setPhoneNumber(String phonenumber) {
        getCustomer().setPhoneNumber(phonenumber);
    }

    public String getPostAddress() {
        return getCustomer().getPostAddress();
    }

    public void setPostAddress(String postaddress) {
        getCustomer().setPostAddress(postaddress);
    }

    public String getPostCode() {
        return getCustomer().getPostCode();
    }

    public void setPostCode(String postaddress) {
        getCustomer().setPostCode(postaddress);
    }

    public String getUserName() {
        return iMatDataHandler.getUser().getUserName();
    }

    public void setUserName(String userName) {
        iMatDataHandler.getUser().setUserName(userName);
    }

    public String getPassword() {
        return iMatDataHandler.getUser().getPassword();
    }

    public void setPassword(String password) {
        iMatDataHandler.getUser().setPassword(password);
    }

    public CreditCard getCreditCard() {
        return iMatDataHandler.getCreditCard();
    }

    public String getCardNumber() {
        return getCreditCard().getCardNumber();
    }

    public void setCardNumber(String cn) {
        getCreditCard().setCardNumber(cn);
    }

    public String getCardType() {
        return getCreditCard().getCardType();
    }

    public void setCardType(String ct) {
        getCreditCard().setCardType(ct);
    }

    public String getCardName() {
        return getCreditCard().getHoldersName();
    }

    public void setCardName(String cn) {
        getCreditCard().setHoldersName(cn);
    }

    public int getValidMonth() {
        return getCreditCard().getValidMonth();
    }

    public void setValidMonth(int m) {
        getCreditCard().setValidMonth(m);
    }

    public int getValidYear() {
        return getCreditCard().getValidYear();
    }

    public void setValidYear(int y) {
        getCreditCard().setValidYear(y);
    }

    public int getVerification() {
        return getCreditCard().getVerificationCode();
    }

    public void setVerification(int cvc) {
        getCreditCard().setVerificationCode(cvc);
    }

    public int getRemovedIndex() {
        return rmIndex;
    }

    public java.util.List<Order> getOrders() {
        return iMatDataHandler.getOrders();
    }

    public java.util.List<Product> getFavorites() {
        return iMatDataHandler.favorites();
    }

    public void addFavorite(Product p) {
        iMatDataHandler.addFavorite(p);
    }

    public void removeFavorite(Product p) {
        iMatDataHandler.removeFavorite(p);
    }

    private java.util.List<ShoppingItem> sci;
    private int rmIndex = 0;
}
