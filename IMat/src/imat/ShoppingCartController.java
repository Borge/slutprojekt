/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imat;

import java.util.ArrayList;
import java.util.List;
import se.chalmers.ait.dat215.project.CartEvent;
import se.chalmers.ait.dat215.project.Product;
import se.chalmers.ait.dat215.project.ShoppingCartListener;
import se.chalmers.ait.dat215.project.ShoppingItem;

/**
 *
 * @author borge
 */
public class ShoppingCartController implements ShoppingCartListener {
    Model model;
    shoppingCartinter cart;
    List<Product> products;
    List<kundVagnNy> items;
    
    public ShoppingCartController(shoppingCartinter shoppingPanel) {
        products = new ArrayList();
        items = new ArrayList();
        model = Model.getInstance();
        model.getShoppingCart().addShoppingCartListener(this);
        cart = shoppingPanel;
        products.add(model.getProduct(1));
        products.clear();
    }


    public void placeOrder() {

        model.placeOrder();
        
    }
    
    public void clearCart() {
        
        model.clearShoppingCart();
        
    }
    
    public void removeItem(){
        int rmIndex = model.getRemovedIndex();
        if(!items.isEmpty()){
            cart.removeItem(items.get(rmIndex));
            items.remove(rmIndex);
            products.remove(rmIndex);
            num--;
        }
    }

    @Override
    public void shoppingCartChanged(CartEvent evt) {
        //System.out.println("hände nåt");
        update();
        while (model.getShoppingCart().getItems().size()>num){
            addItem();
        }
        while (model.getShoppingCart().getItems().size()<num && num>0){
            removeItem();
        }
        if(!model.getShoppingCart().getItems().isEmpty() && model.getShoppingCart().getItems().size() == num && num > 0){

            int indx = model.getShoppingCart().getItems().indexOf(evt.getShoppingItem());
            //System.out.println(indx);
            if(indx >= 0){
                items.get(indx).setAmount((int) evt.getShoppingItem().getAmount());
                items.get(indx).setPrize( evt.getShoppingItem().getProduct().getPrice());
            }
            
        }
    }
    
    public void addItem(){
        imat.kundVagnNy item = new imat.kundVagnNy();
        product = model.getShoppingCart().getItems().get(num).getProduct();
        item.setName(product.getName());

        if(products.isEmpty()){
            products.add(product);
            item.setAmount(1);
            item.setProduct(product);
            items.add(item);
            cart.addItem(item); 
        }
        else if(!products.contains(product)){
            products.add(product);
            item.setAmount(1);
            item.setProduct(product);
            items.add(item);
            cart.addItem(item);
        }
        else{
            int n = products.indexOf(product);
            items.get(n).setAmount(items.get(n).getAmount()+1);
            items.get(n).setPrize(product.getPrice());
            items.add(item);
            products.add(product);
        }
        num++;
    }
    
    private void update() {
        cart.setTotal(model.getShoppingCart().getTotal());
        
    }
    private kundVagnNy vagn;
    private Product product;
    private int num = 0;
}
