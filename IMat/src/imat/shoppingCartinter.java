/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imat;

/**
 *
 * @author borge
 */
public interface shoppingCartinter {
    public void setCustomerName(String name);
    
    public void setPreviousOrders(int numOrders);
    
    public void setTotal(double tot);
    
    public void addItem(imat.kundVagnNy item);
    
    public void removeItem(imat.kundVagnNy item);
    
}
