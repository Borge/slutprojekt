/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imat;

import javax.swing.JLabel;
import se.chalmers.ait.dat215.project.Product;

/**
 *
 * @author borge
 */
public interface ProductPanel {

   
    public JLabel getImageLabel();

    public JLabel getPrizeLabel();

    public JLabel getNameLabel();
    
    public void setFavIcon(Boolean b);
    
    public void setProduct(Product product);
   
}
