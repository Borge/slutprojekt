package imat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author borge
 */
import javax.swing.*;
import se.chalmers.ait.dat215.project.Product;
import se.chalmers.ait.dat215.project.ShoppingItem;

public class ProductController {

    private JLabel imageLabel;
    private JLabel prizeLabel;
    private JLabel nameLabel;
    private Product product;
    int width,height;

    public ProductController(ProductPanel productPanel){
        
	imageLabel = productPanel.getImageLabel();

	if (imageLabel != null){
	    width = imageLabel.getPreferredSize().width;
	    height = imageLabel.getPreferredSize().height;
	}
	prizeLabel = productPanel.getPrizeLabel();
	
	nameLabel = productPanel.getNameLabel();
    }

    public void setProduct(Product product){

	this.product = product;

	update();
    }
    
    public void setAmount(Double d, int i){
        Model.getInstance().getShoppingCart().getItems().get(i).setAmount(d);
        update();
    }
    
    public Product getProduct(){
        return product;
    }
    
    public void removeItem(ShoppingItem sci){
        Model.getInstance().getShoppingCart().removeItem(sci);
    }
    
    public void addToShoppingCart(int i) {

        Model.getInstance().addToShoppingCart(product, i);
        update();

    }
    
    public Double findAmount(){
        for (int i = 0; i<Model.getInstance().getShoppingCart().getItems().size();i++ ){
            if(Model.getInstance().getShoppingCart().getItems().get(i).getProduct().equals(product)){
                return Model.getInstance().getShoppingCart().getItems().get(i).getAmount();
            }
            
        }
        return 1.00;
    }

    private void update(){

	if (prizeLabel != null){
	    prizeLabel.setText(product.getPrice() + " " + product.getUnit());
	}

	if (nameLabel != null){
	    nameLabel.setText(product.getName());
	}

	if (imageLabel != null){
	    imageLabel.setIcon(Model.getInstance().getImageIcon(product, width, height));
	}
    }
}
